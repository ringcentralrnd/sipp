#!/bin/sh
git submodule update --init &&
autoreconf -vifs &&
./configure  --with-pcap "$@" &&
make sipp_unittest &&
./sipp_unittest &&
make
